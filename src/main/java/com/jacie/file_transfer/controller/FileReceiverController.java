package com.jacie.file_transfer.controller;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

@RestController
public class FileReceiverController {

    @Value("${receive.path}")
    private String receivePath;

    @PostMapping("/receive")
    public String receiveFile(@RequestBody MultipartFile file) throws Exception {
        // 获取文件内容
        byte[] fileContent = file.getBytes();

        // 保存文件到目标服务器
        Path filePath = Paths.get(receivePath + file.getOriginalFilename());
        Files.write(filePath, fileContent);

        // 返回响应
        return "文件上传成功！";
    }
}