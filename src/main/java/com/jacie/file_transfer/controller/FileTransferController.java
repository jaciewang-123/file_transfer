package com.jacie.file_transfer.controller;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.FileSystemResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.RequestEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.client.RestTemplate;

import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.nio.file.Files;

@Controller
public class FileTransferController {

    @Value("${transfer.path}")
    private String transferPath;

    @Value("${receive.url}")
    private String receiveUrl;

    @GetMapping("/transfer")
    public ResponseEntity<?> transferFile() throws IOException {
        // 读取要传输的文件
        MultiValueMap<String, Object> multiValueMap = new LinkedMultiValueMap<>();
        // 设置multi/form-data文件
        multiValueMap.add("file", new FileSystemResource(transferPath));
        multiValueMap.add("name", "测试材料");

        // 发送文件请求
        RestTemplate restTemplate = new RestTemplate();
        String response = restTemplate.postForObject(receiveUrl, multiValueMap, String.class);

        // 返回接收到的响应
        return ResponseEntity.ok().body(response);
    }
}